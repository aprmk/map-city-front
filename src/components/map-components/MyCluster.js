import React, {useEffect, useState} from 'react';
import {loadModules} from "esri-loader";

const MyCluster = (props) => {
    const [myMyCluster, setMyCluster] = useState(null);

    function isWithinScaleThreshold() {
        return props.view.scale > 5000;
    }

    useEffect(() => {
        loadModules(['esri/smartMapping/labels/clusters', 'esri/core/promiseUtils'])
            .then(([clusterLabelCreator, promiseUtils]) => {

                const layer = props.map.allLayers.items[props.map.allLayers.items.length - 1]

                function generateClusterConfig(layer) {
                    const labelPromise = clusterLabelCreator
                        .getLabelSchemes({
                            layer: layer,
                            view: props.view
                        })
                        .then(function (labelSchemes) {
                            return labelSchemes.primaryScheme;
                        });

                    return promiseUtils.eachAlways([labelPromise]).then(function (result) {
                        const primaryLabelScheme = result[result.length-1].value;
                        const labelingInfo = primaryLabelScheme.labelingInfo;
                        const clusterMinSize = primaryLabelScheme.clusterMinSize;
                        return {
                            type: "cluster",
                            labelingInfo: labelingInfo,
                            clusterMinSize: clusterMinSize
                        };
                    }).catch(function (error) {
                        console.error(error);
                    });
                }

                layer.when()
                    .then(generateClusterConfig)
                    .then(function (featureReduction) {
                        layer.featureReduction = featureReduction;
                        const toggleButton = document.getElementById("toggle-cluster");
                        toggleButton.addEventListener("click", toggleClustering);

                        function toggleClustering() {
                            if (isWithinScaleThreshold()) {
                                let fr = layer.featureReduction;
                                layer.featureReduction =
                                    fr && fr.type === "cluster" ? null : featureReduction;
                            }
                            toggleButton.innerText =
                                toggleButton.innerText === "Enable Clustering"
                                    ? "Disable Clustering"
                                    : "Enable Clustering";
                        }

                        props.view.whenLayerView(layer).then(function (layerView) {
                            const filterSelect = document.getElementById("filter");
                            filterSelect.addEventListener("change", function (event) {
                                const newValue = event.target.value;

                                const whereClause = newValue
                                    ? "category = '" + newValue + "'"
                                    : null;
                                layerView.filter = {
                                    where: whereClause
                                };
                            });
                        });

                        props.view.watch("scale", function (scale) {
                            if (toggleButton.innerText === "Disable Clustering") {
                                layer.featureReduction = isWithinScaleThreshold() ? featureReduction : null;
                            }
                        })
                    }).catch((err) => console.error(err));
                return function cleanup() {
                    props.view.ui.remove(myMyCluster);
                }
            }, []);

    })
    return null;
}

    export default MyCluster;