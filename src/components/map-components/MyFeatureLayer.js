import {useState, useEffect} from 'react';
import {loadModules} from 'esri-loader';
import React from 'react'
import axios from "axios";


const getAllPropblems = async () => {
    const response = await axios.get("http://localhost:8000/requestAboutProblem")
    return response.data.filter(problem => problem.repairsMade === false)
}

const MyFeatureLayer = (props) => {
    const [myFeatureLayer, setMyFeatureLayer] = useState(null);
    
    useEffect(() => {
        loadModules(['esri/layers/FeatureLayer', 'esri/Graphic']).then(([FeatureLayer, Graphic]) => {

            const categoryFirst = {
                type: "simple-marker",
                color: "#2c48ac",
                width: "0.5px",
                style: "solid"
            };

            const categorySecond = {
                type: "simple-marker",
                color: "#1a7a0f",
                width: "0.5px",
                style: "solid"
            };

            const categoryThree = {
                type: "simple-marker",
                color: "#cf0909",
                width: "0.5px",
                style: "solid"
            };

            const categoryNone = {
                type: "simple-marker",
                color: "#292929",
                width: "0.5px",
                style: "solid"
            };

            const getProblemsPoints = async () => {
                const problems = await getAllPropblems();
                console.log(problems)

                const graphicCollection = problems.map(problem => {
                    const name = problem.category.name;

                    const titleProblem = problem.titleProblem
                    const photo = problem.photo
                    const repairsMade = problem.repairsMade

                    const point = {
                        type: "point",
                        longitude: problem.pointOnMap.coordinates[1],
                        latitude: problem.pointOnMap.coordinates[0]
                    }

                    const markerSymbol = {
                        type: "simple-marker",
                        color: [226, 119, 40]
                    }

                    return new Graphic({
                        geometry: point,
                        symbol: markerSymbol,
                        attributes: {
                            category: name,
                            titleProblem: titleProblem,
                            photo: photo,
                            repairsMade: repairsMade,
                        },
                    })
                })

                return graphicCollection
            }

            const hwyRenderer = {
                type: "unique-value",
                legendOptions: {
                    title: "Freeway type"
                },
                defaultSymbol: categoryNone,
                defaultLabel: "categoryNone",
                field: "category",
                uniqueValueInfos: [
                    {
                        value: "ремонт ям",
                        symbol: categoryFirst,
                        label: "ремонт ям"
                    },
                    {
                        value: "зовнішні фактори перекриття шляху",
                        symbol: categorySecond,
                        label: "зовнішні фактори перекриття шляху"
                    },
                    {
                        value: "відкриті люки",
                        symbol: categoryThree,
                        label: "відкриті люки"
                    },
                    ]
            };

            getProblemsPoints().then((features) => {
                const myFeatureLayerObj = new FeatureLayer({
                    title: "Places of worship",
                    source: features,
                    renderer: hwyRenderer,
                    objectIDField: "ObjectID",
                    graphicCollection: "graphicCollection",
                    fields: [
                        {
                            name: "ObjectID",
                            alias: "ObjectID",
                            type: "oid"
                        },
                        {
                            name: 'category',
                            type: 'string',
                            alias: 'category',
                        },
                        {
                            name: "titleProblem",
                            type: "string",
                        },
                        {
                            name: "photo",
                            type: "blob",
                        },
                        {
                            name: "repairsMade",
                            type: "string",
                        },
                    ],
                    outFields:['category'],
                    popupTemplate:{
                        content:[
                            {
                                type: 'fields',
                                fieldInfos: [
                                    {
                                        fieldName: 'titleProblem',
                                        label: 'Title'
                                    },
                                    {
                                        fieldName: 'category',
                                        label: 'Category'
                                    }
                                ]
                            },
                            {
                                type: 'media',
                                mediaInfos: [{
                                    "type": "image",
                                    "value": {
                                        "sourceURL": "{photo}"
                                    }
                                }]
                            }
                        ]
                    },
                });

                setMyFeatureLayer(myFeatureLayerObj);
                props.map.add(myFeatureLayerObj);
                console.log('MyFeatureLayer', myFeatureLayerObj)
            })
        }).catch((err) => console.error(err));
        return function cleanup() {
            props.map.remove(myFeatureLayer);
        }
    }, []);

    return null
}

export default MyFeatureLayer;