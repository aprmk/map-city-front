import  { useState, useEffect } from 'react';
import { loadModules } from 'esri-loader';

const MyMapView = (props) => {

    const [myMapView, setMyMapView] = useState(null);
    useEffect(() => {
        loadModules(['esri/views/MapView']).then(([MapView]) => {
            const myMapViewObj = new MapView({
                view: props.view,
                container: "viewDiv",
            });

            setMyMapView(myMapViewObj);

            // const infoDiv = document.getElementById("infoDiv");
            myMapView.view.on("click", function(event) {
                let lat = Math.round(event.mapPoint.latitude * 1000) / 1000;
                let lon = Math.round(event.mapPoint.longitude * 1000) / 1000;

                console.log(lat,lon )

                const coord = [lat,lon]

                props.onClick(coord)

            });
            // props.map.add(myLegend);
        }).catch((err) => console.error(err));
        return function cleanup() {
            props.view.remove(myMapView);
        }
    }, [ props ]);

    return null;
}

export default MyMapView;