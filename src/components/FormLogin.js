import {styles} from "./FormStyles";
import React, {useEffect, useState} from 'react';
import axios from "axios";
import useToken from "./useToken";


const loginUser = async (cred) => {
    const response = await axios.post('http://localhost:8000/auth/login', cred)
    return response.data;
}


const FormLogin = () => {
    const [login, setLogin] = useState('')
    const [password, setPassword] = useState('')

    const [loginError, setLoginError] = useState('NOT EMPTY')
    const [passwordError, setPasswordError] = useState('NOT EMPTY')

    const [formValid, setFormValid] = useState(false)

    const [loginDirty, setLoginDirty] = useState(false)
    const [emailDirty, setEmailDirty] = useState(false)
    const [passwordDirty, setPasswordDirty] = useState(false)


    const { setToken } = useToken();

    const handleSubmit = async (e) => {
        try {
            e.preventDefault();
            const data = await loginUser({
                login,
                password
            });
            if(data.accessToken){
                setToken(data);
                window.location.href = "/profile"

            }
        }
        catch (err) {
            alert( "User with current login or password doesn't exist" );
        }

    }

    useEffect(() => {
        if (loginError || passwordError) {
            setFormValid(false)
        } else {
            setFormValid(true)
        }
    }, [loginError, passwordError])

    const loginHandler = (e) => {
        setLogin(e.target.value)
        if (e.target.value.length < 3 || e.target.length > 20)
        {
            setLoginError('Login longer than 3 and shorter than 20')
            if (!e.target.value) {
                setLoginError('Login cannot be empty')
            }
        } else {
            setLoginError('')
        }
    }

    const passwordHandler = (e) => {
        setPassword(e.target.value)
        if (e.target.value.length < 3 || e.target.length > 20)
        {
            setPasswordError('Password longer than 3 and shorter than 20')
            if (!e.target.value) {
                setPasswordError('Password cannot be empty')
            }
        } else {
            setPasswordError('')
        }
    }

    const blurHandler = (e) => {
        switch (e.target.name) {
            case 'login':
                setLoginDirty(true)
                break
            case 'email':
                setEmailDirty(true)
                break
            case 'password':
                setPasswordDirty(true)
                break
        }
    }

    return (
        <div className='container_form_large'>
            <form onSubmit={handleSubmit}>
                <div className='container_form_little'>
                    <h2>Login</h2>
                    <label className='child_form'>
                        {(loginDirty && loginError)}

                        <p>Login</p>

                        <input style={styles.input}

                               type='text'
                               onChange={e => loginHandler(e)} value={login}
                               onBlur={e => blurHandler(e)} name='login'
                        />
                    </label>
                    <label className='child_form'>
                        {(passwordDirty && passwordError)}

                        <p>Password</p>

                        <input style={styles.input}

                               type='password'
                               onChange={e => passwordHandler(e)} value={password}
                               onBlur={e => blurHandler(e)} name='password'
                        />
                    </label>
                    <div >
                        <a href="/register" >Want to register ?</a>
                    </div>
                    <button disabled={!formValid} className='btn btn_login' type='submit'>Log-in</button>
                </div>
            </form>
        </div>

    );
};

export default FormLogin;