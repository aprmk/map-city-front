export const styles = {
    input: {
        padding: "10px",
        width: "92%",
        borderRadius: "5px",
        fontSize: "18px",
        backgroundColor: "#eef0f6",
        border: "1px solid rgba(110,110,110,.3)",
        "color": "black",
    }
}