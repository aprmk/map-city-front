import React, {useState} from 'react';
import Switch from 'react-input-switch';
import axios from "axios";
import {Grid} from "@material-ui/core";

const ProblemItem = ({data}) => {

    const [repairsMade, setRepairsMade] = useState(data.repairsMade)

    const handleChange = async (value) => {
        setRepairsMade(Boolean(value))

        await updateProblem(Boolean(value))
    }

    const updateProblem = async (newValue) =>{
        try {
            const res = await axios.put(`http://localhost:8000/requestAboutProblem/visibility/${data.id}`,{repairsMade: newValue})
            return res.data;
        } catch (error) {
            throw new Error(`Unable to get category that use `);
        }
    }

    return (
        <div style={{borderBottom: "1px inset #b7b3b3"}}>
            <div className="problem-item">
                <label className="problem-title">Name problem:</label>
                <div className="problem-data">{data.titleProblem}</div>
            </div>

            <div className="problem-item">
                <label className="problem-title">Is problem solved ? </label>
                <div className="problem-data">
                    <div style={{padding: "0 5px", margin: "-3px 0"}}>No</div>
                    <Switch
                    value={+repairsMade}
                    onChange={async (e) => await handleChange(e)}
                    />
                    <div style={{padding: "0 5px", margin: "-3px 0"}}>Yes</div>
                </div>

            </div>
        </div>
    );
};

export default ProblemItem;