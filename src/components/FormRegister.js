import React, { useState, useEffect} from 'react';
import {styles} from "./FormStyles";
import axios from "axios";

const registerUser = async (cred) => {
    await axios.post('http://localhost:8000/auth/register', cred)
}

const FormRegister = () => {
    const [email, setEmail] = useState('')
    const [login, setLogin] = useState('')
    const [password, setPassword] = useState('')
    // const [successful, setSuccessful] = useState(false)

    const [loginError, setLoginError] = useState('NOT EMPTY')
    const [emailError, setEmailError] = useState('NOT EMPTY')
    const [passwordError, setPasswordError] = useState('NOT EMPTY')

    const [formValid, setFormValid] = useState(false)

    const [loginDirty, setLoginDirty] = useState(false)
    const [emailDirty, setEmailDirty] = useState(false)
    const [passwordDirty, setPasswordDirty] = useState(false)

    const handleSubmit = async (e) => {
        e.preventDefault();
        // setSuccessful(false);
        await registerUser({
            login,
            email,
            password
        });
    }

    useEffect(() => {
        if (loginError || emailError || passwordError) {
            setFormValid(false)
        } else {
            setFormValid(true)
        }
    }, [loginError, emailError, passwordError])

    const loginHandler = (e) => {
        setLogin(e.target.value)
        if (e.target.value.length < 3 || e.target.length > 20)
        {
            setLoginError('Login longer than 3 and shorter than 20')
            if (!e.target.value) {
                setLoginError('Login cannot be empty')
            }
        } else {
            setLoginError('')
        }
    }

    const emailHandler = (e) => {
        setEmail(e.target.value)
        const regular = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        if (!regular.test(String(e.target.value).toLowerCase())) {
            setEmailError('Email must have @')
        } else {
            setEmailError("")
        }
    }

    const passwordHandler = (e) => {
        setPassword(e.target.value)
        if (e.target.value.length < 3 || e.target.length > 20)
        {
            setPasswordError('Password longer than 3 and shorter than 20')
            if (!e.target.value) {
                setPasswordError('Password cannot be empty')
            }
        } else {
            setPasswordError('')
        }
    }

    const blurHandler = (e) => {
        switch (e.target.name) {
            case 'login':
                setLoginDirty(true)
                break
            case 'email':
                setEmailDirty(true)
                break
            case 'password':
                setPasswordDirty(true)
                break
        }
    }

    return (
        <div className='container_form_large'>
            <form onSubmit={handleSubmit}>
                <div className='container_form_little'>
                    <label className='child_form'>
                        {(loginDirty && loginError)}

                        <p>Login</p>

                        <input style={styles.input}

                               type='login'
                               onChange={e => loginHandler(e)} value={login}
                               onBlur={e => blurHandler(e)} name='login'
                        />
                    </label>
                    <label className='child_form'>
                        {(emailDirty && emailError)}

                        <p>Email</p>

                        <input style={styles.input}

                               type='email'
                               onChange={e => emailHandler(e)} value={email}
                               onBlur={e => blurHandler(e)} name='email'
                        />
                    </label>
                    <label className='child_form'>
                        {(passwordDirty && passwordError)}

                        <p>Password</p>

                        <input style={styles.input}

                               type='password'
                               onChange={e => passwordHandler(e)} value={password}
                               onBlur={e => blurHandler(e)} name='password'

                        />
                    </label>
                    <button disabled={!formValid} className='btn btn_login' type='submit'>Login-up</button>
                </div>
            </form>
        </div>

    );
};

export default FormRegister;