import React, {useEffect, useState} from 'react';
import {Map} from '@esri/react-arcgis';
import MyFeatureLayer from "../components/map-components/MyFeatureLayer";
import MyLegend from "../components/map-components/MyLegend";
import MyCluster from "./map-components/MyCluster";
import axios from "axios";
import MyMapView from "./map-components/MyMapView";
import ModalRequest from "./ModalRequest";
import useToken from "./useToken";



const MapCity = () => {
    const [category, setCategory] = useState([]);
    const [modalIsOpen, setModalOpen] = useState(false);

    const [map, setMap] = useState(null);
    const [view, setView] = useState(null);

    const {getToken} = useToken();

    const handleMapLoad = (map, view) => {
        setMap(map)
        setView(view)
    }

    const getCategoryProblem = async () => {

        try {
            const res = await axios.get('http://localhost:8000/category')
            return res.data.map(category => category.name);

        } catch (error) {
            throw new Error(`Unable to get category that use `);
        }
    };
    useEffect(() =>{
        getCategoryProblem()
            .then(res => setCategory(res))
    }, [])

    const showModal = () => {


       setModalOpen(true)
    }

    const  closeModal = () => {
        setModalOpen(false)
    }

    return (
        <div className="map_city">
            <div className="button-map-container">
                <button className="btn other-form-button button-map" type="button" onClick={showModal} disabled={!getToken()}>Add problem</button>
            </div>

            <Map
                key={'map'}
                onLoad={handleMapLoad}
                style={{width: '100vw', height: '80vh'}}
                mapProperties={{basemap: 'osm'}}
                viewProperties={{
                center: [ 25.9344444444, 48.2908333333 ],
                zoom: 12
            }}
                >

                <MyFeatureLayer
                    key={'dhgg'}
                    featureLayerProperties={{
                    }}
                >
                    <MyMapView onClick={showModal}/>
                    {/*<MyCluster/>*/}
                </MyFeatureLayer>
                <MyCluster/>
                <MyLegend/>
            </Map>
           <ModalRequest modalIsOpen={modalIsOpen} modalIsClose={closeModal} view={view} setModalIsOpen={setModalOpen}/>
            <div id="infoDiv" className="esri-widget" >
                Filter by category:

                <select id="filter" className="esri-select" defaultValue={""}>
                    {category.map(category_one => {
                       return  <option key={category_one} value={category_one}>{category_one}</option>
                    })}
                    <option key={"all"} value="">All</option>
                </select>
                <div>
                    <button id="toggle-cluster" className="esri-button">Disable filter categories</button>
                </div>
                <div id="legendDiv"/>
            </div>
        </div>
    );
};

export default MapCity;

// [25.9344444444, 48.2908333333]